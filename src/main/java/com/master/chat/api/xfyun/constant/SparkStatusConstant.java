package com.master.chat.api.xfyun.constant;

/**
 * 讯飞星火常量
 *
 * @author: Yang
 * @date: 2023/12/1
 * @version: 1.0.0
 * Copyright Ⓒ 2023 MasterComputer Corporation Limited All rights reserved.
 */
public interface SparkStatusConstant {
    /**
     * 回复结束
     */
    Integer FINISH = 2;

    /**
     * 开始
     */
    Integer REPLY = 0;

}
