package com.master.chat.common.enums;

/**
 * 数据源
 *
 * @author: Yang
 * @date: 2021/10/20
 * @version: 1.2.0
 * Copyright Ⓒ 2023 Master Computer Corporation Limited All rights reserved.
 */
public enum DataSourceTypeEnum {

    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
